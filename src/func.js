/* eslint-disable require-jsdoc */
const {listOfPosts,listOfPosts2}=require('../src/posts');
const getSum = (str1, str2) => {
  if ( typeof str1!='string' ||  typeof str2!='string') {
    return false;
  }
  str1 = +str1;
  str2 = +str2;
  
  if (isNaN(str1) || isNaN(str2)) {
    return false;
  }
  return (str1 + str2).toString();
};
// console.log(getSum('123', '123'));



const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  const posts = listOfPosts.filter((post) => post.author === authorName);
  let allComments = [];
  listOfPosts.forEach((post) => {
    if (post.comments) {
      allComments = [...allComments, ...post.comments];
    }
  });
  const comments = allComments.filter((comment) => comment.author === authorName);
  return `Post:${posts.length},comments:${comments.length}`;
};
console.log(getQuantityPostsByAuthor(listOfPosts2, 'Rimus'));
console.log(getQuantityPostsByAuthor(listOfPosts2, 'Ivanov'));
console.log(getQuantityPostsByAuthor(listOfPosts2, 'Uncle'));
console.log(getQuantityPostsByAuthor(listOfPosts2, 'Lena'));

function tickets(people) {
  let clerk = [];
  let canGiveChange = true;
  people.forEach((bill) => {
    clerk = countChange(clerk, bill);
    if (clerk) {
      clerk.push(bill);
    } else {
      canGiveChange = false;
    }
  });
  
  //console.log(clerk)
  //return clerk;
  return canGiveChange ? 'YES' : 'NO';
}


function countChange(clerk, bill) {
  if (!clerk) {
    return false;
  }
  switch (bill) {
    case 50:
      return giveChange(clerk, 25);
      break;
    case 100:
      if (clerk.includes(50)) {
        clerk=giveChange(clerk, 50);
        return giveChange(clerk, 25);
      } else {
        clerk=giveChange(clerk, 25);
        clerk=giveChange(clerk, 25);
        return giveChange(clerk, 25);
      }

      break;
    case 25:
      return clerk;
      break;
    default:
      return false;
  }
}


function giveChange(array, bill) {
  if (!array) {
    return false;
  }
  const index = array.indexOf(bill);
  if (index == -1) {
    return false;
  }
  array.splice(index, 1);
  return array;
}

console.log(tickets([25, 50, 100]));
console.log(tickets([25, 50, 25, 25, 100]));
console.log(tickets([25, 50, 25, 50, 25, 50, 25, 100]));
console.log(tickets([35, 25, 50, 100]));
console.log(tickets([25, 25, 50, 50, 25, 100]));


module.exports =  {getSum, getQuantityPostsByAuthor, tickets} ;